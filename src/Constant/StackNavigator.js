import React from "react";
import { createStackNavigator } from 'react-navigation-stack';


import Login from '../screens/Login/View/Login';
import Registration from '../screens/Registration/View/Registration';
import ConfirmationCode from '../screens/ConfirmationCode/View/ConfirmationCode';
import PhoneNumber from '../screens/PhoneNumber/View/PhoneNumber';
import EditProfile from '../screens/EditProfile/View/EditProfile';
import ChangePassword from '../screens/ChangePassword/View/ChangePassword';
import Settings from '../screens/Settings/View/Settings';
import Complaints from '../screens/Complaints/View/Complaints';
import ResetPassword from '../screens/ResetPassword/View/ResetPassword';
import CompanyProfile from '../screens/CompanyProfile/View/CompanyProfile';
import WorkerProfile from '../screens/WorkerProfile/View/WorkerProfile';
import AboutApp from '../screens/AboutApp/View/AboutApp';
import Home from '../screens/Home/View/Home';
import Worker from '../screens/Worker/View/Worker';
import Companies from '../screens/Companies/View/Companies';
import Favorites from '../screens/Favorites/View/Favorites';
import Souq from '../screens/Souq/View/Souq';
import Category from '../screens/Category/View/Category';
import Shop from '../screens/Shop/View/Shop';
import Product from '../screens/Product/View/Product';
import DeliverySerivce from '../screens/DeliverySerivce/View/DeliverySerivce';

import SplashScreen from '../screens/SplashScreen';
import StatusBarColor from "../components/StatusBarColor";
import { createAppContainer } from "react-navigation";
import { Root } from 'native-base';

const Stack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator 
    screenOptions={{
        headerStyle: {
          backgroundColor: "#9AC4F8",
        },
        headerTintColor: "white",
        headerBackTitle: "Back",
        initialRouteName: 'Splash',
      }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Registration" component={Registration} />
      <Stack.Screen name="ConfirmationCode" component={ConfirmationCode} />
      <Stack.Screen name="PhoneNumber" component={PhoneNumber} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="Settings" component={Settings} />
      <Stack.Screen name="Complaints" component={Complaints} />
      <Stack.Screen name="ResetPassword" component={ResetPassword} />
      <Stack.Screen name="WorkerProfile" component={WorkerProfile} />
      <Stack.Screen name="CompanyProfile" component={CompanyProfile} />
      <Stack.Screen name="AboutApp" component={AboutApp} />
      <Stack.Screen name="Worker" component={Worker} />
      <Stack.Screen name="Companies" component={Companies} />
      <Stack.Screen name="Favorites" component={Favorites} />
      <Stack.Screen name="Souq" component={Souq} />
      <Stack.Screen name="Category" component={Category} />
      <Stack.Screen name="Shop" component={Shop} />
      <Stack.Screen name="Product" component={Product} />
      <Stack.Screen name="DeliverySerivce" component={DeliverySerivce} />
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
    </Stack.Navigator>
  );
  
}

export { MainStackNavigator };