import React, { Component } from 'react';
import { View, StyleSheet, Text } from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { screenWidth, screenHeight, appFontBold, AppColor, MediumGrey } from './Styles';
import { strings } from '../screens/i18n';
import { Button } from 'native-base';

export default class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }



  render() {
    return (

      <View style={styles.container}>
        <Button onPress={() => this.props.navigation.navigate("Home")} transparent block style={styles.iconcontainer}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Home")}
          >
          </TouchableOpacity>
          <Text numberOfLines={1} style={this.props.current == 'Home' ? styles.activeText : styles.text}>{(strings('lang.Home'))}</Text>
        </Button>

        <View style={{ width: 2, marginHorizontal: "1%", height: "70%", alignSelf: "center", backgroundColor: "#eee" }}></View>

        <Button onPress={() => this.props.navigation.navigate("Settings")} transparent block style={styles.iconcontainer}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Settings")}
          >
          </TouchableOpacity>
          <Text numberOfLines={1} style={this.props.current == 'Settings' ? styles.activeText : styles.text}>{(strings('lang.setting'))}</Text>
        </Button>


      </View>

    )

  }

};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
    height: screenHeight / 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: screenWidth / 20,
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    borderColor: "#eee",
    borderWidth: 1
  },
  iconcontainer: {
    width: '45%',
    height: 50,
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center', alignSelf: "center"
  },

  text: {
    color: MediumGrey,
    fontFamily: appFontBold,
    fontSize: screenWidth / 36
  },
  activeText: {
    color: AppColor,
    fontFamily: appFontBold,
    fontSize: screenWidth / 30,
  },
});


