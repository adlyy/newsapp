import React from 'react';
import { View, StyleSheet, Image, Text, I18nManager, Platform } from "react-native";
import { Button, } from 'native-base';
import { screenHeight, appFontBold, AppColor } from './Styles';

const Header = (props) => (
    <View style={styles.header}>
        {props.back &&
            <View style={{ width: "25%", alignSelf: 'center' }}>
                <Button onPress={() => props.back()} style={styles.backBtn}>
                    {I18nManager.isRTL ?
                        <Image source={require("../images/back_2.png")} style={styles.img} />
                        :
                        <Image source={require("../images/9.png")} style={styles.img} />
                    }
                </Button>
            </View>
        }

        <View style={styles.titlecontainer}>
            <Text style={styles.title}>{props.title}</Text>
        </View>
    </View>
)

const styles = StyleSheet.create({
    header: {
        width: "100%", flexDirection: "row", justifyContent: 'flex-start',
        alignSelf: 'center', height: screenHeight / 9, backgroundColor: "transparent", paddingHorizontal: '4%',
        borderBottomWidth:1,borderColor:AppColor
    },
    backBtn: {
        alignSelf: "flex-start", width: 30, height: 30, justifyContent: "center",
        marginTop: Platform.OS == 'ios' ? '7%' : '6%', marginLeft: '3%', backgroundColor: AppColor
    },

    titlecontainer: {
        width: "50%", alignSelf: 'center', justifyContent: 'center', marginTop: '2%'
    },
    title: {
        fontFamily: appFontBold, color: AppColor, textAlign: 'center'
    },
    img: {
        width: "90%", height: "90%", resizeMode: "contain", alignSelf: "center", tintColor: "#fff"
    }
});
export default Header;


