import React from 'react';
import { View, StyleSheet, } from 'react-native';
import { Text } from "native-base";
import { screenHeight, medium, AppColor, appFontBold } from './Styles';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
const News = (props) => (
  <TouchableOpacity onPress={() => props.navigation.navigate("NewsDetails", { news: props.news })}>
    <View style={styles.containerView}>
      <FastImage
        style={styles.img}
        source={{
          uri: props.news.urlToImage ? props.news.urlToImage : '',
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{props.news.title}</Text>
      </View>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  containerView: {
    width: "95%", alignSelf: 'center', flexDirection: 'column', borderRadius: 5,
    marginVertical: '1%', height: screenHeight / 3, backgroundColor: "#fff", justifyContent: "center", padding: "3%"
  },
  img: {
    width: '100%', height: '60%', borderRadius: 5, borderWidth: 1, borderColor: 'transparent', alignSelf: "center"
  },
  titleContainer: {
    width: "100%", height: "40%", paddingVertical: "2%", justifyContent: "center"
  },
  title: {
    fontFamily: appFontBold, textAlign: "left", fontSize: medium, lineHeight: 20, color: AppColor
  }

});
export default News;