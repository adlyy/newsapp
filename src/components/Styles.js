import { StyleSheet, Dimensions, Platform, StatusBar ,I18nManager} from "react-native";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
export const { height: screenHeight, width: screenWidth } = Dimensions.get('window');
export const appFontBold = 'Cairo-Bold'
export const appFontMedium = 'Cairo-Medium'
export const appFont = 'Cairo-Regular'
export const appFontSemiBold = 'Cairo-SemiBold'



export const DarkGrey = '#707070'
export const borderColor = '#C8C7CC'
export const black = '#292D32'
export const backgroundColor = '#e3e3e3'
export const AppColor = '#003980'
export const Background = '#E5E5E5'
export const placeHolder = '#535353'

export const Small = screenWidth / 30
export const medium = screenWidth / 24
export const big = screenWidth / 22



const styles = StyleSheet.create({
    statusBar: {
      height: STATUSBAR_HEIGHT / 10
    },
})


export default styles
